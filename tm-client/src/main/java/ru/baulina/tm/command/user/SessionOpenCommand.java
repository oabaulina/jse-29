package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.bootstrap.Bootstrap;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public class SessionOpenCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "open-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Open session.";
    }

    @Override
    public void execute() {
        System.out.println("[OPEN_SESSION]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        final SessionDTO session = endpointLocator.getSessionEndpoint().openSession(login, password);
        @NotNull final Bootstrap bootstrap = (Bootstrap) endpointLocator;
        bootstrap.setSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
