package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.command.AbstractCommand;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.NumberUtil;
import ru.baulina.tm.util.SystemInformation;

public class ServerInfoCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-iSer";
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about server.";
    }

    @Override
    public void execute() {
        System.out.println("[SERVER INFO]");

        @Nullable final SessionDTO session = getSession();
        System.out.println("HOST: " + endpointLocator.getAdminUserEndpoint().getHost(session));
        System.out.println("PORT: " + endpointLocator.getAdminUserEndpoint().getPort(session));
        System.out.println("[OK]");
        System.out.println();
    }


}
