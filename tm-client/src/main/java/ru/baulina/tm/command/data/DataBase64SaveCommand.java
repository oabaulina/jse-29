package ru.baulina.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.exception.user.AccessDeniedException;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    @Override
    public String name() {
        return "data-base64-save";
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to base64 file.";
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @Nullable final SessionDTO session = getSession();
        if (session == null) throw new AccessDeniedException();
        endpointLocator.getAdminDampEndpoint().dataBase64Save(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
