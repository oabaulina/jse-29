package ru.baulina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.baulina.tm.command.AbstractCommand;

import java.util.Arrays;
import java.util.List;

public final class CommandShowCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    public void execute() {
        @NotNull final List<String> commands = serviceLocator.getCommandService().getCommandsNames();
        System.out.println(Arrays.toString(commands.toArray()));
        System.out.println();
    }

}
