package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER NAME");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getProjectEndpoint().removeOneProjectByName(session, name);
        System.out.println("[OK]");
        System.out.println();
    }

}
