package ru.baulina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "unlocked";
    }

    @NotNull
    @Override
    public String description() {
        return "Unlocked user.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCKED_USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        @Nullable final SessionDTO session = getSession();
        endpointLocator.getAdminUserEndpoint().unlockUserLogin(session, login);
        System.out.println("[OK]");
        System.out.println();
    }

}
