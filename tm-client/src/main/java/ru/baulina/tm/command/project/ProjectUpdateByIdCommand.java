package ru.baulina.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.ProjectDTO;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String name() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id.";
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("ENTER ID:");
        @Nullable final Long id  = TerminalUtil.nexLong();
        @Nullable final SessionDTO session = getSession();
        @Nullable final ProjectDTO project = endpointLocator.getProjectEndpoint().findOneProjectById(
                session, session.getUserId()
        );
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NAME:");
        @Nullable final String name  = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description  = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().updateProjectById(session, id, name, description);
        System.out.println("[OK]");
        System.out.println();
    }

}
