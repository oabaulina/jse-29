package ru.baulina.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.entity.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull List<E> findAll();

    void setClazz(Class<E> clazzToSet);

    void merge(@Nullable final List<E> entities);

    void merge(@Nullable final E... entities);

    void merge(@Nullable final E entity);

    void save(E entity);

    void update(E entity);

    void clear();

    @NotNull Long count();

}
